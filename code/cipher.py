def encrypt(text, s):
    result = ""
    # transverse the plain text
    for i in range(len(text)):
        char = text[i]
        # Encrypt uppercase characters in plain text
        if char.isupper():
            result += chr((ord(char) + s - 65) % 26 + 65)
        # Encrypt lowercase characters in plain text
        else:
            result += chr((ord(char) + s - 97) % 26 + 97)
    return result

while True:
    # Demande à l'utilisateur d'entrer le texte à chiffrer
    text = input("Entrez le texte à chiffrer (ou 'q' pour quitter) : ")
    
    if text.lower() == 'q':
        break
    
    # Demande à l'utilisateur d'entrer le décalage de chiffrement
    while True:
        try:
            s = int(input("Entrez le décalage de chiffrement (un entier) : "))
            break
        except ValueError:
            print("Le décalage de chiffrement doit être un entier.")

    print("Texte en clair : " + text)
    print("Décalage de chiffrement : " + str(s))
    print("Texte chiffré : " + encrypt(text, s))
    print()  # saut de ligne pour une meilleure lisibilité
    