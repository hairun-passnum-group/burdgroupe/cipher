from flask import Flask
from flask import render_template
from flask import request

app = Flask(__name__)


@app.route('/', methods=['GET', 'POST'])
def home():
    if request.method == 'POST':
        text = request.form['text']
        if text not in [None, '']:
            result = "test"
            return render_template('index.html', data={
                'text': text,
                'result': result 
            })
    return render_template('index.html')


if __name__ == '__main__': 
    app.run()